const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.coin
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {

      const label = Math.floor(Math.random() * 2) + 1 === 1
                  ? 'heads'
                  : 'tails';
      resolve(`coin landed on ${label}`);

    });
  }
};

module.exports = Command;
