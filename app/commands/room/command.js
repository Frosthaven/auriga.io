const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.room
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.discord = discord;
      this.bot     = discord.bot;
      this.storage = this._auriga.storage;
      this.register();
      setTimeout(() => {
        this._auriga.registerTask('room-purge', 1, this.purgeOldRooms.bind(this));
      }, 5000);
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    if (service._name !== 'discord') {
      return Promise.reject();
    }

    const userID    = bundle.userID;
    const channelID = bundle.channelID;
    const channel   = this.bot.channels.cache.get(channelID);

    if (!channel || channel.type === 'dm') {
      return Promise.resolve('You must be in a valid server to run this command.');
    }

    return new Promise((resolve, reject) => {
      const prefix       = this._auriga.config.prefix;
      const serverID     = channel.guild.id;
      const messageParts = this._auriga.extractCommandFromString(message);
      const displayName  = this.discord.getDisplayNameByID(serverID, userID);

      let payload;
      switch(messageParts.cmd.toLowerCase()) {
        case 'x':
          // remove a room
          payload = {
            userID: userID,
            serverID: serverID
          };
          this.getRoom(payload)
            .then(this.removeRoom.bind(this))
            .then(resolve)
            .catch(resolve);
          break;

        case 'invite':
          // invite people to private room
          payload = {
            userID: userID,
            serverID: serverID
          };
          this.getRoom(payload)
            .then((payload) => {
              const users   = bundle.source.mentions.users.map(x => x.id);
              const roles   = bundle.source.mentions.roles.map(x => x.id);
              const invites = users.concat(roles);

              payload.roomID = payload.channel.roomID;
              if (
                payload &&
                payload.channel &&
                payload.channel.privacy === 'public'
              ) {
                resolve(`Your room is a public room which doesn't need invitations!`);
              } else if (users.length === 0 && roles.length === 0) {
                resolve(`You didn\'t invite anyone! use \`${prefix}room invite @user @user\``);
              } else {
                const processInvites = () => {
                  payload.userID = invites.shift();
                  this.addUserToChannelPermissions(payload)
                  .then(() => {
                    if (invites.length === 0) {
                      resolve('All Invites Sent');
                    } else {
                      processInvites();
                    }
                  }).catch(this.warn);
                };
                processInvites();
              }
            })
            .catch(resolve);
          break;

        case 'private':
          // create a private room
          payload = {
            name: (messageParts.str === '')
                  ? `⚿ ${displayName}'s room`
                  : '⚿ ' + messageParts.str,
            userID: userID,
            serverID: serverID,
            privacy: 'private'
          };
          this.ensureUserRoomDoesntExist(payload)
            .then(this.createRoom.bind(this))
            .then(this.moveChannelToTop.bind(this))
            .then(this.provisionAsPrivate.bind(this))
            .then(this.addUserToChannelPermissions.bind(this))
            .then((payload) => {
              resolve(`Channel \`${payload.name}\` created! You can use \`${prefix}room x\` to remove it manually at any time. Use \`${prefix}room invite @user @user\` to invite people!`);
            })
            .catch(resolve);
          break;

        default:
          // create a public room
          payload = {
            name: (message === '')
                  ? `🗪 ${displayName}'s room`
                  : `🗪 ${message}`,
            userID: bundle.userID,
            serverID: bundle.serverID,
            privacy: 'public'
          };

          this.ensureUserRoomDoesntExist(payload)
            .then(this.createRoom.bind(this))
            .then(this.moveChannelToTop.bind(this))
            .then((payload) => {
              resolve(`Channel \`${payload.name}\` created! You can use \`${this._auriga.config.prefix}room x\` to remove it manually at any time.`);
            })
            .catch(resolve);
          break;
      }
    });
  }

  /**
   * creates a public voice chat room
   * @param {object} payload an object containing parameters
   * @param {string} payload.name the name of the room
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  createRoom(payload) {
    return new Promise((resolve, reject) => {
      this.getRoomCategory(payload).then((catID) => {
        const guild = this.bot.guilds.cache.get(payload.serverID);
        const opts = {
          type: 'voice',
          position: 0,
          lockPermissions:false
        };
        if (this.bot.channels.cache.get(catID)) {
          opts.parent = catID;
        }
        guild.channels.create(`${payload.name}`, opts)
        .then(channel => {
          this.storage.insert({
            type: 'user-room',
            privacy: payload.privacy,
            userID: payload.userID,
            serverID: payload.serverID,
            roomID: channel.id,
            created: Date.now()
          }, (err, resp) => {
            if (err) {
              reject(err);
            } else {
              payload.roomID = channel.id;
              resolve(payload);
            }
          });
        });
      });
    });
  }

  /**
   * removes a voice room from the server
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  removeRoom(payload) {
    return new Promise((resolve, reject) => {
      this.storage.remove({
        type: 'user-room',
        userID: payload.userID,
        serverID: payload.serverID
      }, (err, numRemoved) => {
        if (err) {
          reject(err);
        } else {
          const channel = this.bot.channels.cache.get(payload.channel.roomID);
          if (channel) {
            channel.delete();
          }
          resolve('room successfully deleted');
        }
      });
    });
  }

  /**
   * gets an assigned room category for the server from config.yaml
   * @param {object} payload an object containing parameters
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  getRoomCategory(payload) {
    return new Promise((resolve, reject) => {
      if (
        this._settings.serverRoomCategoryID &&
        this._settings.serverRoomCategoryID[payload.serverID]
      ) {
        resolve(this._settings.serverRoomCategoryID[payload.serverID]);
      } else {
        resolve(false);
      }
    });
  }

  /**
   * retrieves a user's room
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves on success, rejects on errors
   */
  getRoom(payload) {
    return new Promise((resolve, reject) => {
      this.storage.findOne(
        {type: 'user-room', userID: payload.userID, serverID: payload.serverID},
        (err, room) => {
          if (err) {
            reject(err);
          } else if (room) {
            payload.channel = room;
            resolve(payload);
          } else {
            reject('You do not currently have any rooms open on this server');
          }
        }
      );
    });
  }

  /**
   * fetches a user's room and ensures it doesn't exist
   * @param {object} payload an object containing parameters
   * @param {string} payload.name the name of the room
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @return {promise} resolves with registered room if it exists
   */
  ensureUserRoomDoesntExist(payload) {
    return new Promise((resolve, reject) => {
      this.storage.findOne(
        {type: 'user-room', userID: payload.userID, serverID: payload.serverID},
        (err, room) => {
          if (room) {
            this.getRoom(payload)
            .then(this.removeRoom.bind(this))
            .then(resolve(payload));
          } else {
            resolve(payload);
          }
        }
      );
    });
  }

  /**
   * moves a channel to the top of the list
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @param {object} payload.roomID the id of the target voice channel
   * @return {promise} resolves on success, rejects on errors
  */
  moveChannelToTop(payload) {
    return new Promise((resolve, reject) => {
      const guild = this.bot.guilds.cache.get(payload.serverID);
      const channel = guild.channels.cache.get(payload.roomID);
      channel.edit({position:0})
      .then(resolve(payload))
      .catch(resolve(payload));
    });
  }

  /**
   * makes a room private
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.serverID the id of the target server
   * @param {string} payload.roomID the channel id
   * @return {promise} resolves on success, rejects on errors
  */
  provisionAsPrivate(payload) {
    return new Promise((resolve, reject) => {
      const channel = this.bot.channels.cache.get(payload.roomID);
      channel.overwritePermissions([
        {
          id: payload.serverID,
          deny: [
            'CONNECT',
            'SPEAK',
            'MUTE_MEMBERS',
            'DEAFEN_MEMBERS',
            'MOVE_MEMBERS',
            'USE_VAD',
            'CREATE_INSTANT_INVITE',
            'MANAGE_ROLES',
            'MANAGE_CHANNELS']
        }
      ], 'Bot Provisioning')
      .then(resolve(payload))
      .catch((err) => {
        reject(err);
        this.warn(err);
      });
    });
  }

  /**
   * adds a user to channel permissions
   * @param {object} payload an object containing parameters
   * @param {string} payload.userID the user's id
   * @param {string} payload.roomID the channel id
   * @return {promise} resolves on success, rejects on errors
  */
  addUserToChannelPermissions(payload) {
    return new Promise((resolve, reject) => {
      const channel = this.bot.channels.cache.get(payload.roomID);
      channel.updateOverwrite(
        payload.userID,
        {
          'CONNECT':        true,
          'SPEAK':          true,
          'MUTE_MEMBERS':   true,
          'DEAFEN_MEMBERS': true,
          'USE_VAD':        true
        },
        'Bot Provisioning'
      )
      .then(resolve(payload))
      .catch((err) => {
        this.warn(err);
        reject(err);
      });
    });
  }

  /**
   * @todo
   * purges old rooms
   */
  purgeOldRooms() {
    const minUsers   = 1;
    const checkAfter = 1;
    const now        = Date.now();
    this.storage.find({type: 'user-room'}, (err, rooms) => {
      rooms.forEach((room) => {
        const channel = this.bot.channels.cache.get(room.roomID);
        if (channel && channel.members) {
          const memberCount = channel.members.map(x => x.id).length;
          const age = (now - parseInt(room.created)) / 1000 / 60;
          if (memberCount < minUsers && age >= checkAfter) {
            // delete the channel
            channel.delete().then(() => {
              // delete the database entry
              this.storage.remove({
                type:'user-room',
                roomID: room.roomID
              }, {}, (err, numRemoved) => {
              });
            });
          }
        }
      });
    });
  }
};

module.exports = Command;
