const BasicCommand = use('classes/BasicCommand');
const fs           = require('fs');
const util         = require('util');
const du           = require('du');

/**
 * @alias Command.mem
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    this._auriga.loadService('webLocalServer').then((web) => {
      this.webIcons = web.registerStatic(`${__dirname}/icons`, this);
    });
    this.register();
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'discord') {
        reject();
      }

      this.getFolderDiskUsage(process.cwd()).then((size) => {
        const rss       = util.inspect(process.memoryUsage().rss);
        const heapUsed  = util.inspect(process.memoryUsage().heapUsed);
        const heapTotal = util.inspect(process.memoryUsage().heapTotal);
        const codeStack = rss - heapTotal;
        const dbSize    = fs.statSync(this._auriga.config.storage)['size'];

        let embedObj = {
          type: 'rich',
          title: 'Performance Metrics',
          description: '[Node Memory Diagram](http://i.stack.imgur.com/I188N.png)',
          color: 0xff7200,
          fields: []
        };

        if (this.webIcons) {
          embedObj.thumbnail = {
            url: `${this.webIcons}/perf.png?v=1`
          };
        } else {
          console.log('icons not defined');
        }

        // memory usage field
        embedObj.fields.push({
          name: 'Memory Usage',
          value: `Total:                ${this.formatBytes(rss, 0)}\nCode & Stack: ${this.formatBytes(codeStack, 0)}\nHeap Use:        ${this.formatBytes(heapUsed, 0)}`,
          inline: true
        });

        // disk usage field
        embedObj.fields.push({
          name: 'Disk Usage',
          value: `Total:          ${this.formatBytes(size, 0)}\nPlatform:    ${this.formatBytes(size - dbSize, 0)}\nDatabase:  ${this.formatBytes(dbSize, 0)}`,
          inline:true
        });

        // scheduled tasks
        if (this._auriga.tasks) {
          const tasks = Object.keys(this._auriga.tasks).map((x) => {
            return `${x} ${this._auriga.tasks[x].interval}m`;
          }).sort();
          embedObj.fields.push({
            name: `Scheduled Tasks (${tasks.length})`,
            value: tasks.join(', ')
          });
        }

        // platform statistics
        const services = Object.keys(this._auriga.services).map(x => x).sort();
        const plugins  = Object.keys(this._auriga.plugins).map(x => x).sort();
        const commands = Object.keys(this._auriga.commands).map(x => x).sort();

        embedObj.fields.push({
          name: `Services (${services.length})`,
          value: services.join(', ')
        });
        embedObj.fields.push({
          name: `Plugins (${plugins.length})`,
          value: plugins.join(', ')
        });
        embedObj.fields.push({
          name: `Commands (${commands.length})`,
          value: commands.join(', ')
        });

        // complete!
        resolve({
          to: bundle.channelID,
          embed: embedObj
        });
      })
      .catch((err) => {
        reject(this.error(err));
      });
    });
  }

  /**
   * gets the disk usage of a particular folder on the system
   * @param {string} path the disk folder path to check for size
   * @return {promsie} resolves with size, or rejects with error
   */
  getFolderDiskUsage(path) {
    return new Promise((resolve, reject) => {
      du(`${process.cwd()}`, (err, size) => {
        if (err) {
          reject(err);
        } else {
          resolve(size);
        }
      });
    });
  }

  /**
   * converts bytes into a readable string
   * @param  {number} bytes    [size in bytes]
   * @param  {number} decimals [number of decimal places]
   * @return {string}          [the formatted string]
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
        return '0 Byte';
    }
    const k = 1000;
    const dm = decimals + 1 || 3;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes/Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
};

module.exports = Command;
