const BasicCommand = use('classes/BasicCommand');
const shortNumber = require('short-number');

/**
 * @alias Command.ranks
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    Promise.all([
      this._auriga.loadPlugin('discordStats')
    ]).then((requirements) => {
      this.stats = requirements[0];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      // don't support direct messages
      let channel = this.stats.bot.channels.cache.get(bundle.channelID);
      if (!channel || !channel.guild || !channel.guild.id) {
        resolve('This must be run from within a valid server');
      }

      const server  = this.stats.bot.guilds.cache.get(channel.guild.id);

      if (
        server
        && this.stats._settings.levelUp
        && this.stats._settings.levelUp[server.id]
        && this.stats._settings.levelUp[server.id].ranks
      ) {
        const ranks = this.stats._settings.levelUp[server.id].ranks.map(
          (x, i) => {
            return `${x.rank} (${shortNumber(x.postReq)}+)`;
          }
        );
        let embedObj = {
          type: 'rich',
          title: `Message Ranks for ${server.name}`,
          description: 'These are ranks earned by chatting on this server',
          color: 0xFED838,
          fields:[{
            name: 'Ranks',
            value: ranks.join('\n')
          }]
        };

        resolve({
          to: bundle.channelID,
          message: ``,
          embed: embedObj
        });
      } else {
        resolve('no ranks found for this server');
      }
    });
  }
};

module.exports = Command;
