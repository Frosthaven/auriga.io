const BasicCommand = use('classes/BasicCommand');
const request      = require('request');

/**
 * @alias Command.anime
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    this._auriga.loadService('webLocalServer').then((web) => {
      this.webIcons = web.registerStatic(`${__dirname}/icons`, this);
    });
    this.register();
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only run on discord
      if (service._name !== 'discord') {
        reject();
      }

      // determine user provided parameters {query, listMode, selected}
      let params = this.parseMessageParams(message);
      if (params.query === '') {
        resolve('You didn\'t search for anything!');
      } else {
        this.searchAnime(params.query).then((results) => {
          if (params.listMode) {
            resolve(this.buildListResponse(bundle, results));
          } else {
            resolve(this.buildEntryResponse(bundle, results, params.selected));
          }
        });
      }
    });
  }

  /**
   * fetches anime information from kitsu.io servers
   * @param {string} query the search terms
   * @return {promise} resolves on success, rejects on errors
   */
  searchAnime(query) {
    return new Promise((resolve, reject) => {
      const reqOptions = {
        url: `https://kitsu.io/api/edge/anime?filter[text]=${encodeURIComponent(query)}`
      };

      request(reqOptions, (error, response, body) => {
        if (!response.body || error) {
          reject('Couldn\'t find the anime you were searching for');
        } else {
          const body = JSON.parse(response.body);
          if (body && body.data) {
            const results = body.data;
            resolve(results);
          } else {
            reject('Couldn\'t find the anime you were searching for');
          }
        }
      });
    });
  };

  /**
   * builds a list response
   * @param {object} bundle the service bundle provided to the command by aurigo
   * @param {array} results the array of results provided by kitsu.io api
   * @return {object} an object response to resolve back to the service
   */
  buildListResponse(bundle, results) {
    let embedObj = this.getEmbedTemplate();
    let list        = '';
    let counter     = 0;
    const resultSet =  results.slice(0, 10);
    resultSet.forEach((entry) => {
      counter++;
      let title = '';
      if (entry.attributes.titles.en
      && entry.attributes.titles.en !== '') {
        title = entry.attributes.titles.en;
      } else if (entry.attributes.titles.en_jp
      && entry.attributes.titles.en_jp !== '') {
        title = entry.attributes.titles.en_jp;
      } else {
        title = entry.attributes.titles.ja_jp;
      }
      list += `\`${counter}\`. ${title}\n`;
    });
    embedObj.title       = `Anime Search Results`;
    embedObj.description = `Use the \`--1\`, \`--2\` (etc) flag ` +
    `at the end of your search query to select a specific entry ` +
    `from the list.\n\n${list}\n _ _`;

    return {
      to: bundle.channelID,
      embed: embedObj
    };
  }

  /**
   * builds an entry response
   * @param {object} bundle the service bundle provided to the command by aurigo
   * @param {array} results the array of results provided by kitsu.io api
   * @param {number} selected the selected entry index
   * @return {object} an object response to resolve back to the service
   */
  buildEntryResponse(bundle, results, selected=0) {
    // get the active index
    let activeIndex = 0;
    if (selected) {
      activeIndex = results[selected] ? selected : 0;
    } else {
      activeIndex = 0;
    }

    // prepare the embed
    let embedObj           = this.getEmbedTemplate();
    const entry            = results[activeIndex];
    embedObj.url           = `https://kitsu.io/anime/${entry.attributes.slug}`;
    embedObj.thumbnail.url = entry.attributes.posterImage.large;

    if (entry.attributes.titles.en && entry.attributes.titles.en !== '') {
      embedObj.title = entry.attributes.titles.en;
      embedObj.description = (entry.attributes.titles.en_jp !== '')
                             ? entry.attributes.titles.en_jp
                             : entry.attributes.titles.ja_jp;
    } else if (
      entry.attributes.titles.en_jp
      && entry.attributes.titles.en_jp !== ''
    ) {
      embedObj.title = entry.attributes.titles.en_jp;
    } else {
      embedObj.title = entry.attributes.titles.ja_jp;
    }

    // field: airing dates
    const startedAiring  = entry.started_airing ? entry.startedAiring : '?';
    const finishedAiring = entry.finished_airing ? entry.finished_airing : '?';
    if (entry.started_airing || entry.finished_airing) {
      embedObj.fields.push({
        name: 'Airing Dates',
        value: `\`${startedAiring}\` to \`${finishedAiring}\``,
        inline: true
      });
    }

    // field: synopsis
    const maxLength = 1014;
    let synopsis    = entry.attributes.synopsis;
    if (synopsis.length > maxLength) {
      synopsis = `${synopsis.slice(0, maxLength - 6)} [...]`;
    }
    embedObj.fields.push({
      name: 'Synopsis',
      value: `${synopsis}\n _ _`,
      inline: false
    });

    return {
      to: bundle.channelID,
      embed: embedObj
    };
  }

  /**
   * returns the default embed object template
   * @return {object} the embed object template
   */
  getEmbedTemplate() {
    let template = {
      thumbnail: {
        url: ''
      },
      color: 0x1ABC9C,
      fields: [],
      footer: {
        text: 'Details provided by kitsu.io'
      }
    };

    if (this.webIcons) {
      template.footer.icon_url = `${this.webIcons}/logo.png`;
    }

    return template;
  }

  /**
   * parses raw user input as message with optional listing parameters
   * @param {string} input the raw user input query
   * @return {object} an object containing relevant parameters
   */
  parseMessageParams(input) {
    let params = {
      query: input,
      listMode: false,
      selected: false
    };

    const selectData  = input.match(/\-\-(\d+)$/);

    if (input.endsWith('--list')) {
      params.listMode = true;
      params.query  = input.replace(/\-\-list/g, '');
    } else if (selectData) {
      params.selected = selectData[0].replace(/\-/g, '') - 1;
      params.query  = input.replace(selectData[0], '');
    }
    params.query = params.query.trim().toLowerCase();

    return params;
  }
};

module.exports = Command;
