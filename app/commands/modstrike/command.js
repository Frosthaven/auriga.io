const BasicCommand = use('classes/BasicCommand');
const moment       = require('moment');

/**
 * @alias Command.modstrike
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadService('discord').then((discord) => {
      this.storage = this._auriga.storage;
      this.discord = discord;
      this._auriga.registerTask('strike-purge', 60, this.purgeOldStrikes.bind(this));
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {object} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      // don't support direct messages
      let channel = this.discord.bot.channels.cache.get(bundle.channelID);
      if (!channel || !channel.guild || !channel.guild.id) {
        resolve('This must be run from within a valid server');
      }

      let by           = bundle.userID;
      let messageParts = this._auriga.extractCommandFromString(message);
      let action       = messageParts.cmd;
      let extra        = messageParts.str;
      let inputParts   = this._auriga.extractCommandFromString(extra);
      let identifier   = inputParts.cmd;
      let reason       = inputParts.str;
      if (!bundle.source.channel || !bundle.source.channel.id) {
        resolve('You must use this command from a valid server.');
      }
      let serverID     = bundle.source.channel.guild.id;
      let username;
      switch(action) {
        case 'find':
          username = this.discord.getDisplayNameByID(serverID, identifier);
          this.findStrikes(serverID, identifier).then((strikes) => {
            /** @todo */
            const max = 10;
            const total = strikes.length;
            strikes   = strikes.slice(0, max).map(x => {
              const dateM = moment.utc(x.date);
              return `\`${x._id}\`: \`${dateM.fromNow()}\` : ${x.reason}\n`;
            }).reverse();
            let message = (`\`${username}\` has ${total} strike(s). Showing up to ${max} most recent:\n\n${strikes.join('')}`);
            resolve(message);
          }).catch(resolve);
          break;
        case 'add':
          username = this.discord.getDisplayNameByID(serverID, identifier);
          this.addStrike(serverID, identifier, by, reason)
          .then((count) => {
            resolve(`strike \`${count}\` has been added and \`${username}\` has been privately messaged the reason.`);
          }).catch(resolve);
          break;
        case 'remove':
          this.deleteStrike(serverID, identifier).then(() => {
            resolve(`strike deleted`);
          }).catch(resolve);
          break;
        case 'clear':
          username = this.discord.getDisplayNameByID(serverID, identifier);
          this.clearStrikes(serverID, identifier).then(() => {
            resolve(`cleared all strikes against \`${username}\``);
          });
          break;
        default:
          resolve('no valid action provided. Actions allowed: `find` `add` `remove` `clear`');
          break;
      }
    });
  }

  /**
   * adds a strike against a user to the database
   * @param {string} serverID discord serverID
   * @param {string} identifier discord user id, name, or partial name, or mention
   * @param {string} by the discord user id of the user submitting the strike
   * @param {string} reason the reason to add to the database
   * @return {promise} resolves with total strikes, rejects on errors
   */
  addStrike(serverID, identifier, by, reason) {
    return new Promise((resolve, reject) => {
      const userID = this.discord.resolveUserID(serverID, identifier);
      if (reason.length < 1) {
        reject('No reason provided');
      } else if (!userID || !this.discord.bot.users.cache.get(userID)) {
        reject(`Couldn't find the requested user`);
      } else {
        this.storage.insert(
          {type:'modstrike', userID:userID, serverID:serverID, reason:reason, date:Date.now()},
        (err) => {
          if (err) {
            reject(err);
          } else {
            this.discord.respond({
              channelID: userID,
              response: {
                to: userID,
                message: `A moderation strike has been added to your account. This strike will expire in ${this._settings.age} days. Please see server rules for more information.\n\`\`\`ACTION: Moderation Strike\nSERVER: ${this.discord.bot.guilds.cache.get(serverID).name}\n\nREASON: ${reason}\`\`\``,

              }
            });
            this.storage.count(
              {type:'modstrike', userID:userID, serverID:serverID},
              (err, count) => {
                let found = count || 0;
                resolve(found);
            });
          }
        });
      }
    });
  }

  /**
   * finds strikes that match the search query
   * @param {string} serverID discord serverID
   * @param {string} identifier discord user id, name, or partial name, or mention
   * @return {promise} resolves with strikes on success, rejects on errors
   */
  findStrikes(serverID, identifier) {
    return new Promise((resolve, reject) => {
      const userID = this.discord.resolveUserID(serverID, identifier);
      if (!userID || !this.discord.bot.users.cache.get(userID)) {
        reject(`Couldn't find the requested user`);
      } else {
        this.storage.find(
          {type:'modstrike', serverID:serverID, userID:userID},
          (err, strikes) => {
          if (err || strikes.length === 0) {
            reject('No strikes were found for this user');
          } else {
            strikes.sort((a, b) => {
              if (a.date > b.date)
                return -1;
              if (a.date < b.date)
                return 1;
              return 0;
            });
            resolve(strikes);
          }
        });
      }
    });
  }

  /**
   * deletes strikes that have the provided ID
   * @param {string} serverID discord serverID
   * @param {string} id the strike _id as stored in the database
   * @return {promise} resolves on success, rejects with message on errors
   */
  deleteStrike(serverID, id) {
    return new Promise((resolve, reject) => {
      id = id.trim();
      this.storage.remove({type: 'modstrike', serverID:serverID, _id: id}, {},
      (err, numRemoved) => {
        if (err || numRemoved < 1) {
          reject('couldn\'t remove the chosen strike');
        } else {
          resolve();
        }
      });
    });
  }

  /**
   * deletes strikes that have the provided ID
   * @param {string} serverID discord serverID
   * @param {string} identifier discord user id, name, or partial name, or mention
   * @return {promise} resolves on success, rejects with message on errors
   */
  clearStrikes(serverID, identifier) {
    return new Promise((resolve, reject) => {
      const userID = this.discord.resolveUserID(serverID, identifier);
      if (!userID || !this.discord.bot.users.cache.get(userID)) {
        reject(`Couldn't find the requested user`);
      } else {
        this.storage.remove({type: 'modstrike', serverID:serverID, userID: userID}, {multi:true},
        (err, numRemoved) => {
          resolve();
        });
      }
    });
  }

  /**
   * purges strikes that are older than the age value in settings
   */
  purgeOldStrikes() {
    const now   = Date.now();
    const prior = now - (86400000 * this._settings.age);
    this.storage.find({type: 'modstrike', date:{$lt: prior}}, (err, strikes) => {
      strikes.forEach((strike) => {
        console.log(`deleting strike ${strike.reason}`);
        this.storage.remove({type:'modstrike', _id:strike._id}, {multi:true});
      });
    });
  }

};

module.exports = Command;
