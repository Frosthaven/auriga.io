const BasicCommand = use('classes/BasicCommand');
const moment       = require('moment'); require('moment-timezone');

/**
 * @alias Command.weather
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadPlugin('darkSkyWeatherApi')
    ]).then((requirements) => {
      this.weather   = requirements[0];
      this._auriga.loadService('webLocalServer').then((web) => {
        this.webIcons = web.registerStatic(`${__dirname}/icons`, this);
      });
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'discord') {
        reject();
      }

      let celcius = false;
      if (message.endsWith('--c')) {
        celcius = true;
        message = message.replace(/\-\-c/g, '').trim();
      }
      this.weather.fetchWeather(message)
      .then((response) => {
        const momentNow = moment(new Date()).tz(response.timezone);
        let embedObj    = this.getEmbedTemplate(response, momentNow);

        embedObj = this.decorateEmbedWithAlerts(response, embedObj);
        embedObj = this.decorateEmbedWithCurrentConditions(
          response,
          embedObj,
          celcius
        );
        embedObj = this.decorateEmbedWithForecast(
          response,
          embedObj,
          celcius,
          momentNow
        );

        resolve({
          to: bundle.channelID,
          embed: embedObj
        });
      })
      .catch(resolve);
    });
  }

  /**
   * adds forecast entries to the embed object
   * @param {object} response the response from the darksky api
   * @param {object} embedObj the discord embed object
   * @param {boolean} celcius whether to calculate in celcius or not
   * @param {object} moment an instance of momentJS
   * @return {object} the updated discord embed object
   */
  decorateEmbedWithForecast(response, embedObj, celcius, moment) {
    let daysAdded = 0;
    response.daily.data.forEach((day) => {
      if (daysAdded < 5) {
        daysAdded++;
        const momentDay = daysAdded === 1 ? moment: moment.add(1, 'day');
        const dayName = daysAdded === 1 ? `Today` : momentDay.format('dddd');

        let dayMin = celcius ? `${Math.round((day.temperatureMin-32)*5/9)}°C` : `${Math.round(day.temperatureMin)}°F`;
        let dayMax = celcius ? `${Math.round((day.temperatureMax-32)*5/9)}°C` : `${Math.round(day.temperatureMax)}°F`;

        embedObj.fields.push({
          name: `${dayName}`,
          value: `${dayMin} / ${dayMax}\n${(day.humidity * 100).toFixed(0)}% Humidity\n${(day.precipProbability * 100).toFixed(0)}% Precipitation\n${this.weather.concatSummary(day.summary)}\n _ _`,
          inline: true
        });
      }
    });

    return embedObj;
  }

  /**
   * adds current conditions to the embed object
   * @param {object} response the response from the darksky api
   * @param {object} embedObj the discord embed object
   * @param {boolean} celcius whether to calculate in celcius or not
   * @return {object} the updated discord embed object
   */
  decorateEmbedWithCurrentConditions(response, embedObj, celcius) {
    let curTemp = celcius ? `${Math.round((response.currently.temperature-32)*5/9)}°C` : `${Math.round(response.currently.temperature)}°F`;
    embedObj.fields.push({
      name: 'Current',
      value: `${curTemp}\n${(response.currently.humidity * 100).toFixed(0)}% Humidity\n${(response.currently.precipProbability * 100).toFixed(0)}% Precipitation\n${this.weather.concatSummary(response.currently.summary)}\n_ _`,
      inline: true
    });

    return embedObj;
  }

  /**
   * adds weather alerts to the embed object
   * @param {object} response the response from the darksky api
   * @param {object} embedObj the discord embed object
   * @return {object} the updated discord embed object
   */
  decorateEmbedWithAlerts(response, embedObj) {
    if (response.alerts && response.alerts.length > 0) {
      let alertLines = '';
      response.alerts.forEach((alert)=> {
        // alertLines += `[${alert.title}](${alert.uri})`;
        alertLines += `\n${alert.title}`;
      });

      embedObj.fields.push({
        name: `IMPORTANT ALERTS`,
        value: alertLines,
        inline: false
      });
    }

    return embedObj;
  }

  /**
   * gets the discord embed template for displaying weather
   * @param {object} response the response from the darksky api
   * @param {object} moment an instance of momentJS
   * @return {object} the discord embed object
   */
  getEmbedTemplate(response, moment) {
    const template = {
      type: 'rich',
      title: `Weather for ${response.address}`,
      description: `[Powered by Dark Sky](https://darksky.net/poweredby/)\n${response.hourly.summary} ${response.daily.summary} [Click here to see the map](https://maps.darksky.net/@temperature,${response.latitude},${response.longitude},5).`,
      color: 0x9c00cf,
      fields:[],
      footer: {
        text: `${moment.format('h:mma dddd, MM-DD-YYYY')} (${response.timezone})`,
      }
    };

    if (this.webIcons) {
      template.thumbnail = {
        url: `${this.webIcons}/${response.currently.icon}.png`
      };
      template.footer.icon_url = `${this.webIcons}/clock.png`;
    }

    return template;
  }
};

module.exports = Command;
