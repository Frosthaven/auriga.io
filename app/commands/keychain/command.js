const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.keychain
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
    ]).then((requirements) => {
      this.discord = requirements[0];
      this.register();
    });
  }
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only run on discord
      if (service._name !== 'discord') {
        reject();
      }

      const command = this._auriga.extractCommandFromString(message).cmd;

      if (
        this._auriga.commands[command]
        && this._auriga.commands[command]._settings
        && this._auriga.commands[command]._settings.keychain
      ) {
        const rawKeychain = this._auriga.commands[command]._settings.keychain;
        const keychain    = this.discord.translateKeychain(rawKeychain, bundle);
        resolve(`:key: \`${command}\` ${this.discord.codeBlock('json', keychain)}`);
      } else {
        resolve(`Couldn't find keys for \`${command}\``);
      }
    });
  }
};

module.exports = Command;
