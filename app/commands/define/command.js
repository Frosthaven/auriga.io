const BasicCommand = use('classes/BasicCommand');
const cheerio      = require('cheerio');
const request      = require('request');

/**
 * @alias Command.define
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the plugin code when called
   */
  init() {
    this._auriga.loadService('webLocalServer').then((web) => {
      this.webIcons = web.registerStatic(`${__dirname}/icons`, this);
    });
    this.register();
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'discord') {
        reject();
      }

      this.getDefinition(message).then((response) => {
        let template = {
          to: bundle.channelID,
          message: `<@${bundle.userID}>`,
          embed: {
            title: response.word,
            description: `${response.definitions
              .slice(0, 4)
              .join('\n\n')
              .replace(/\:/g, '\`\:\` ')}\n_ _
            `,
            color: 0x4f545c,
            footer: {
              text: 'Provided by Merriam-Webster',
            }
          }
        };

        if (this.webIcons) {
          template.embed.footer.icon_url = `${this.webIcons}/logo.png?v=1`;
        }

        resolve(template);
      }).catch((err) => {
        this.warn(err);
        resolve(err);
      });
    });
  }

  /**
   * retrieves a definition from merrium webster
   * @param {string} query the text search query
   * @return {promise} resolves if ok, rejects on errors
   */
  getDefinition(query) {
    const self     = this;
    const endpoint = `http://www.dictionaryapi.com/api/v1/references/collegiate/xml/${query}?key=${this._settings.dictionary_key}`;
    return new Promise((resolve, reject) => {
      request(endpoint, (error, response, body) => {
        let $ = cheerio.load(response.body);
        const word = $('ew').html();
        if (typeof word === 'string') {
          let responseObj = {
            word: word,
            definitions: []
          };

          $('def').eq(0).find('dt').each(/* @this HTMLElement */ function(i, elem) {
            let def = self.filterResponse($(this).html());
            if (!def.startsWith(':with respect to') && !def.startsWith(':with reference to')) {
              responseObj.definitions.push(def);
            }
          });

          resolve(responseObj);
        } else {
          reject('couldn\'t find that word in the dictionary');
        }
      });
    });
  }

  /**
   * Filters a response to strip meta tags from definitions
   * @param {string} response the response provided from mw api call
   * @return {string} filtered response
   */
  filterResponse(response) {
    return response.replace(/<sx\>/g, '').replace(/<\/sx\>/g, '')
    .replace(/<d_link\>/g, '').replace(/<\/d_link\>/g, '')
    .replace(/<it\>/g, '').replace(/<\/it\>/g, '')
    .replace(/<g\>/g, '').replace(/<\/g\>/g, '')
    .replace(/<cat\>/g, '').replace(/<\/cat\>/g, '')
    .replace(/<ca\>/g, '').replace(/<\/ca\>/g, '')
    .replace(/<fw\>/g, '[').replace(/<\/fw\>/g, ']')
    .replace(/<un\>/g, '[').replace(/<\/un\>/g, ']')
    .replace(/<dx\>[\s\S]*<\/dx\>/g, '')
    .replace(/<dxt\>[\s\S]*<\/dxt\>/g, '')
    .replace(/<sxn\>[\s\S]*<\/sxn\>/g, '')
    .replace(/<vi\>[\s\S]*<\/vi\>/g, '')
    .replace(/\&\#x273D\;/g, '∗')
    .replace(/\&apos\;/g, '\'')
    .replace(/\&amp\;/g, '&');
  }
};

module.exports = Command;
