const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.streamlotto
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this._auriga.loadPlugin('twitchApi').then((twitchapi) => {
      this.twitchapi = twitchapi;
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      if (service._name !== 'twitch') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      let includeStaff = false;
      if (message.indexOf('+staff') !== -1) {
        includeStaff = true;
      }

      const channelUser = bundle.channel.replace('#', '');
      this.twitchapi.getChatViewers(channelUser).then((collection) => {
        let viewers = collection.viewers;

        if (includeStaff) {
          viewers = viewers.concat(
            collection.moderators,
            collection.staff,
            collection.admins,
            collection.global_mods
          );
        }

        if (viewers.length <=0) {
          resolve('Nobody to choose from!');
        } else {
          const winner = viewers[Math.floor(Math.random() * viewers.length)];
          resolve(`The user ${winner} was selected!`);
        }
      }).catch(resolve);
    });
  }
};

module.exports = Command;
