const BasicCommand = use('classes/BasicCommand');
const strings      = require('./strings.json');

/**
 * @alias Command.8ball
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      const randomIndex    = Math.floor(Math.random()*strings.responses.length);
      const randomResponse = strings.responses[randomIndex];
      resolve(randomResponse);
    });
  }
};

module.exports = Command;
