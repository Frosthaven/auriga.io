const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.streamreg
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
      this._auriga.loadPlugin('streamRegistry')
    ]).then((requirements) => {
      this.discord  = requirements[0];
      this.registry = requirements[1];
      this.register();
    });
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @param {string} service the service that called the action
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle, service) {
    return new Promise((resolve, reject) => {
      // only support discord for now
      if (service._name !== 'discord') {
        reject(this.error(`unsupported service: ${service._name}`));
      }

      const messageParts = this._auriga.extractCommandFromString(message);
      const words        = messageParts.str.split(' ');
      const channels     = this.filterUnique(words);

      switch(messageParts.cmd.toLowerCase()) {
        case 'add':
          this.registry.addChannels(channels)
          .then(() => {
            this.registry.getChannels()
            .then(this.makeListResponse)
            .then(resolve)
            .catch((err) => {
              this.warn(err);
              reject(err);
            });
          }).catch(resolve);
          break;
        case 'remove':
          this.registry.removeChannels(channels)
          .then(() => {
            this.registry.getChannels()
            .then(this.makeListResponse)
            .then(resolve)
            .catch((err) => {
              this.warn(err);
              reject(err);
            });
          }).catch(resolve);
          break;
        case 'list':
          this.registry.getChannels()
          .then(this.makeListResponse)
          .then(resolve)
          .catch((err) => {
            this.warn(err);
            reject(err);
          });
          break;
        case 'update':
          this.registry.update().then(() => {
            resolve('Update processed!');
          }).catch(resolve);
        default:
          break;
      }
    });
  }

  /**
   * removes all duplicate entries in an array
   * @param {array} a an array of entries
   * @return {array} the filtered array with only unique entries
   */
  filterUnique(a) {
    return Array.from(new Set(a));
  }

  /**
   * creates and sends a discord response of stream status
   * @param {array} channels an array of channels in storage
   * @return {promise} resolves on success, rejects on errors
   */
  makeListResponse(channels) {
    return new Promise((resolve, reject) => {
      let lines = [];
      channels.forEach((channel) => {
        let indicator = (channel.online) ? '⚪' : '⚫';
        let bot = (channel.usingBot) ? '(+bot)' : '';
        lines.push(`${indicator} ${channel.name} ${bot}`);
      });

      if (channels.length === 0) {
        lines.push('There are currently no registered streams');
      }

      resolve(lines.join(' '));
    });
  }
};

module.exports = Command;

