const BasicCommand = use('classes/BasicCommand');

/**
 * @alias Command.quote
 * @extends _.BasicCommand
 */
class Command extends BasicCommand {
  /**
   * code to run on load
   */
  init() {
    this.lastRandom = null;
    this.register();
  }

  /**
   * runs the command code when called
   * @param {string} message the message received from the service
   * @param {object} bundle the bundled meta data provided by the service
   * @return {promise} resolves with response when finished, rejects on errors
   */
  action(message, bundle) {
    return new Promise((resolve, reject) => {
      this.getRandomQuote().then((quote) => {
        resolve(quote);
      }).catch(resolve);
    });
  }

  /**
   * retrieves a random quote from the database
   * @return {promise} resolves on quote, rejects on errors
   */
  getRandomQuote() {
    return new Promise((resolve, reject) => {
      this.countQuotes().then((count) => {
        let random = Math.floor(Math.random() * count);

        // re-roll on duplicate and we have at least 4 quotes saved
        while (random === this.lastRandom && count >= 4) {
          random = Math.floor(Math.random() * count);
        }

        this._auriga.storage.find({type:'fortis-quote'})
          .skip(random)
          .limit(1)
          .exec((err, res) => {
            if (err) {
              resolve(err);
            } else if (!res[0]) {
              resolve('No quotes found');
            } else {
              const quote = res[0].quote;
              resolve(quote);
            }
          });
      }).catch(() => {
        resolve('No quotes found');
      });
    });
  }

  /**
   * counts the number of quotes in the database
   * @return {promise} resolves on count, rejects on errors
   */
  countQuotes() {
    return new Promise((resolve, reject) => {
      this._auriga.storage.count({type: 'fortis-quote'}, (err, count) => {
        if (err) {
          reject(err);
        } else {
          resolve(count);
        }
      });
    });
  }
};

module.exports = Command;
