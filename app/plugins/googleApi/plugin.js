const BasicPlugin  = use('classes/BasicPlugin');
const youtube      = require('youtube-search');
const GoogleImages = require('google-images');

/**
 * @alias Plugin.googleApi
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {

  /**
   * runs the plugin code when called
   */
  init() {
    this.imageClient = new GoogleImages(this._settings.cse, this._settings.key);
    this.register();
  }

  /**
   * searches youtube videos
   * @param {string} query the search query
   * @param {object} opts an object containing search options
   * @return {promise} resolves on result success, rejects on errors
   */
  youtubeSearch(query, opts) {
    opts.key = this._settings.key;

    if (query === '') {
      return Promise.reject(`You didn't enter anything to search for!`);
    }

    return new Promise((resolve, reject) => {
      youtube(query, opts, (err, results) => {
        if (results && results.length > 0) {
          resolve(results);
        } else {
          reject(`Couldn't find the video you were looking for.`);
        }
      });
    });
  }

  /**
   * searches google images
   * @param {string} query the search query
   * @return {promise} resolves on result success, rejects on errors
   */
  imageSearch(query) {
    const self = this;

    if (query === '') {
      return Promise.reject(`You didn't enter anything to search for!`);
    }

    return new Promise((resolve, reject) => {
      self.imageClient.search(query).then((results) => {
        if (results && results.length > 0) {
          resolve(results);
        } else {
          reject(`Couldn't find the image you were looking for.`);
        }
      });
    });
  }
};

module.exports = Plugin;
