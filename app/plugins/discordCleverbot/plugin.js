const BasicPlugin = use('classes/BasicPlugin');
const Cleverbot = require('cleverbot-node');

/**
 * @alias Plugin.discordCleverbot
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord')
    ]).then((requirements) => {
      this.discord   = requirements[0];

      this.cleverbot = new Cleverbot();
      this.cleverbot.configure({botapi: this._settings.api_key});

      this.registerResponder();
      this.register();
    });
  }

  /**
   * registers a cleverbot responder
   */
  registerResponder() {
    let that = this;
    this.discord.bot.on('message', msg => {
      const userID    = msg.author.id;
      const botID     = this.discord.bot.user.id;
      const channelID = msg.channel.id;

      let message     = msg.content;

      if (message.startsWith(`<@!${botID}> `)) {
        message = message.split(' ').splice(1).join(' ');
        Cleverbot.prepare(() => {
          this.cleverbot.write(message, (response) => {
          try {
            this.discord.respond({
              channelID: channelID,
              response: {
                to: channelID,
                message: `<@${userID}> ${response.output}`
              }
            });
          } catch(err) {
            that.warn(err);
          }
        });
        });
      }
    });
  }

  /**
   * prepares message strings with replacements
   * @param {string} userID the user id of the member who joined
   * @param {string} str the message string to run replacements on
   * @return {string} the transformed string
   */
  prepareString(userID, str) {
    str = str
          .replace(/\%user/g, `<@${userID}>`)
          .replace(/\%here/g, '@here')
          .replace(/\%help/g, `${this._auriga.config.prefix}help`);
    return str;
  }
};

module.exports = Plugin;
