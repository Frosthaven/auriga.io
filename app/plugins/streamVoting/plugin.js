const BasicPlugin = use('classes/BasicPlugin');

/**
 * @alias Plugin.streamVoting
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
  /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('twitch'),
      this._auriga.loadService('webLocalServer')
    ]).then((requirements) => {
      this.twitch  = requirements[0];
      this.web     = requirements[1];
      this.polls   = {};
      this.createRoute();
      this.listenForVotes();
      this.register();
    });
  }

  /**
   * creates the web route
   */
  createRoute() {
    // create get route
    this.web.server.get('/streamWidgets/voting/:twitchChannel', (req, res) => {
      const channelName = req.params.twitchChannel.toLowerCase();

      this.validateChannel(channelName).then(() => {
        res.status(200);
        // res.send(`${channelName} is VALID. We should show the voting widget here!`);
        res.render(`${__dirname}/views/voting.twig`, {
          socketPort: this.web._settings.port,
          channelName: channelName
        });
      }).catch(() => {
        this.warn(`Unauthorized access from channel ${channelName}`);
        res.status(401);
        res.send(`ERROR 401: UNAUTHORIZED.<br /><br /><strong>${channelName}</strong> is not an authorized twitch channel.`);
      });
    });

    // create socket listener
    this.web.socketio.on('connection', (socket) => {
      socket.on('joinChannel', (data) => {
        const socketRoomName = `streamwidgets.voting.${data.channelName}`;
        socket.join(socketRoomName);
        // socket.emit('channelJoined', {channelName:socketRoomName});
        this.web.socketio.emit(socketRoomName).emit('channelJoined', {
          channelName:socketRoomName
        });
      });
    });
  }

  /**
   * checks if a poll is currently open and accepting votes
   * @param {string} channel the twitch channel name
   * @return {boolean} true if a poll is running
   */
  isPollOpen(channel) {
    return this.polls[channel];
  }

  /**
   * opens a poll for the supplied twitch channel
   * @param {string} channel the twitch channel name
   * @param {number} data an object containing poll data
   * @param {number} data.duration the duration in seconds a poll should be open
   * @param {string} data.text the poll text
   * @param {array}  data.options an array of poll options (strings)
   */
  openPoll(channel, data) {
    if (this.isPollOpen(channel)) {
      this.closePoll(channel);
    }

    this.populatePollData(channel, data);
    this.createPollInterval(channel, data);

    const startText = this.generateStartText(channel, data);
    this.twitch.bot.say(channel, startText);
  }

  /**
   * closes a poll for the supplied twitch channel
   * @param {string} channel the twitch channel name
   * @param {object} winningData object holding information about the winner
   * @param {number} winningData.count total votes for the winner
   * @param {string} winningData.text text content for the winning option
   */
  closePoll(channel, winningData = null) {
    if (this.polls[channel].interval) {
      clearInterval(this.polls[channel].interval);
    }
    delete this.polls[channel];
    if (winningData) {
      const message = `/me The poll has ended! "${winningData.text}" wins with ${winningData.count} vote(s)!`;
      this.twitch.bot.say(channel, message);
    }
  }

  /**
   * creates the poll's lifecycle and self-terminates when finished
   * @param {string} channel the twitch channel id
   * @param {number} data an object containing poll data
   * @param {number} data.duration the duration in seconds a poll should be open
   * @param {string} data.text the poll text
   */
  createPollInterval(channel, data) {
    const socketRoomName = `streamwidgets.voting.${channel}`;
    const maxTime        = 1000*data.duration;
    const interval       = 2000;

    this.web.socketio.emit(socketRoomName).emit('votingStarted', data);

    let elapsed = 0;
    this.polls[channel].interval = setInterval(() => {
      elapsed = elapsed + interval;
      const totals = this.polls[channel].totals;
      if (elapsed < maxTime) {
        this.web.socketio.emit(socketRoomName).emit('votingUpdate', {
          totals:this.polls[channel].totals
        });
      } else {
        clearInterval(this.polls[channel].interval);

        // calculate the winning indexes
        const highestValue = Math.max.apply(null, totals);
        const winners = totals.reduce((arr, x, i) => {
          if (x === highestValue) {
            arr.push(i);
          }
          return arr;
        }, []);

        let winningIndex = winners[0];
        if (winners.length > 1) {
          // tie! randomly select a winner.
          winningIndex = winners[Math.floor(Math.random() * winners.length)];
        }

        const winningData = {
          count: this.polls[channel].totals[winningIndex],
          text: data.options[winningIndex]
        };

        this.web.socketio.emit(socketRoomName).emit('votingEnded', {
          totals:this.polls[channel].totals,
          winningIndex: winningIndex
        });
        this.closePoll(channel, winningData);
      }
    }, interval);
  }

  /**
   * Generates the text string for when a poll is started
   * @param {string} channel the twitch channel id
   * @param {number} data an object containing poll data
   * @param {number} data.duration the duration in seconds a poll should be open
   * @param {string} data.text the poll text
   * @return {string} the fully generated start text
   */
  generateStartText(channel, data) {
    let choiceText = '';
    data.options.forEach((x, i) => {
      choiceText+= ` (${i+1}) ${x}`;
    });

    const minutes = data.duration/60;
    return `/me ${Math.round(minutes*10)/10}m to vote: ${data.text}${choiceText}`;
  }

  /**
   * populates initial data for a poll
   * @param {string} channel the twitch channel id
   * @param {number} data an object containing poll data
   * @param {number} data.duration the duration in seconds a poll should be open
   * @param {string} data.text the poll text
   * @param {array}  data.options an array of poll options (strings)
   */
  populatePollData(channel, data) {
    this.polls[channel] = {};
    this.polls[channel].totals = [];
    this.polls[channel].totals.length = data.options.length;
    this.polls[channel].totals.fill(0);
    this.polls[channel].voters = {};
  }

  /**
   * listens to twitch bot chatter for people placing votes
   */
  listenForVotes() {
    this.twitch.bot.on('message', (channel, userstate, message, self) => {
      if (this.isPollOpen(channel)) {
        message = message.trim().replace(/\#/g, '');
        const pattern = /(^[0-9]+$)/g;
        if (
          pattern.test(message)
          && parseInt(message) > 0
          && parseInt(message) <= this.polls[channel]['totals'].length
        ) {
          // a vote has been received
          const username      = userstate.username;
          const voteIndex     = message-1;
          const prevVoteIndex = this.polls[channel]['voters'][username];

          // subtract from totals if the user already voted
          if (parseInt(prevVoteIndex) >= 0) {
            this.polls[channel]['totals'][prevVoteIndex]--;
          }

          // add the user's vote
          this.polls[channel]['totals'][voteIndex]++;
          this.polls[channel]['voters'][username] = voteIndex;
          // console.log(`${username} voted for ${message}!`);
          // console.log(this.polls);
        }
      }
    });
  }

  /**
   * determines if a provided channel name is joined by our twitch bot
   * @param {string} channelName the name of the twitch channel
   * @return {promise} resolves if the bot is connected to the channel
   */
  validateChannel(channelName) {
    return new Promise((resolve, reject) => {
      resolve();
      /* DIRTY PATCH - ALWAYS VALIDATE FOR NOW
      if (this.twitch.bot.getChannels().includes(`#${channelName}`)) {
        resolve();
      } else {
        reject();
      }
      */
    });
  }
};

module.exports = Plugin;
